Django==1.7.6
django-angular==0.7.9
django-suit==0.2.12
djangorestframework==3.1.0
six==1.9.0
