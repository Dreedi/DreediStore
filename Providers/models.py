from django.db import models
from django.utils.text import slugify

class Provider(models.Model):
	'''
        Provider model for database
    '''
	name = models.CharField(
		blank=False,
		max_length=20,
		null=False,
	)

	code = models.CharField(
		blank=False,
		max_length=20,
		null=False,
	)

	contact = models.CharField(
		blank=True,
		max_length=20,
		null=True,
	)

	phone_1 = models.BigIntegerField(
		blank=True,
		null=True,
	)

	phone_2 = models.BigIntegerField(
		blank=True,
		null=True,
	)

	web_page = models.URLField(
		blank=True,
		null=True,
	)

	email = models.EmailField(
		blank=True,
		null=True,
	)

	registration_date = models.DateField(
		auto_now_add=True,
		blank=False,
		null=False,
	)

	slug = models.SlugField(
		blank=True,
		max_length=50,
		null=True,
		unique=True,
	)

	class Meta:
		verbose_name = "Provider"
		verbose_name_plural = "Providers"

	def slugify_name(self):
		return slugify(self.name)

	def save(self, *args, **kwargs):
		self.slug = self.slugify_name()
		super(Provider, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.name