from .models import Provider
from django import forms 
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _


default_errors = {
	'blank':_('El campo esta en blanco'),
	'invalid':_('Inserte un valor valido'),
	'max_length':_('Longitud maxima rebasada'),
	'required':_('Este campo es requerido'),
}

custom_errors = {
	'unique_name':_('Este proveedor ya esta registrado'),
	'unique_code':_('Este codigo ya fue asignado a otro proveedor'),
	'provider_not_exist':_('El provedor no existe'),
}

def validate_blank(value):
	if str(value).isspace():
		raise forms.ValidationError(default_errors['blank'],code='blank')

def validate_name(name,slug):
	validate_blank(name)
	try:
		provider = Provider.objects.get(slug = slug)
	except Provider.DoesNotExist:
		return name
	else:
		raise forms.ValidationError(custom_errors['unique_name'])


def validate_code(code):
	validate_blank(code)
	try:
		provider = Provider.objects.get(code__iexact = code)
	except Provider.DoesNotExist:
		return code
	else:
		raise forms.ValidationError(custom_errors['unique_code'])

def validate_redirect_provider(name):
	validate_blank(name)
	try:
		provider = Provider.objects.get(slug = slugify(name))
	except Provider.DoesNotExist:
		raise forms.ValidationError(custom_errors['provider_not_exist'])
	else:
		return name