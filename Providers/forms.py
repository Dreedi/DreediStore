from .models import Provider
from .validators import validate_name, validate_code, default_errors, validate_blank, validate_redirect_provider
from django import forms
from djangular.forms import NgModelFormMixin, NgFormValidationMixin
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

class ProviderForm(NgFormValidationMixin, NgModelFormMixin, forms.ModelForm):
	'''
	    Form for update and create providers
	'''
	scope_prefix = 'provider'
	form_name = 'providerform'

	class Meta:
		model = Provider
		exclude = ['slug']
		labels = {
		    'name': _('Nombre'),
		    'code': _('Codigo'),
		    'contact': _('Contacto'),
		    'phone_1': _('Telefono 1'),
		    'phone_2': _('Telefono 2'),
		    'web_page': _('Pagina Web'),
		    'email': _('Email'),
		}
		help_texts = {
		    'name': _('Nombre del Proveedor'),
		    'code': _('Codigo de identificacion'),
		    'contact': _('Contacto con el Proveedor'),
		    'phone_1': _('Telefono 1'),
		    'phone_2': _('Telefono 2'),
		    'web_page': _('Pagina Web del Proveedor'),
		    'email': _('Email de contacto del Proveedor'),
		}

	def clean_name(self):
		original_name = self.cleaned_data.get('name')
		slug_name = slugify(original_name)
		return validate_name(original_name,slug_name)
		
	def clean_code(self):
		code = self.cleaned_data.get('code')
		return validate_code(code)

	def __init__(self, *args, **kwargs):
	    super(ProviderForm, self).__init__(*args, **kwargs)
	    for field in self.fields:
	        self.fields[field].error_messages.update(default_errors)
	        self.fields[field].validators=[validate_blank]

class SearchProviderForm(forms.ModelForm):
	class Meta:
		model = Provider
		fields = ['name']
		labels = {
		    'name': _('Nombre'),
		}
		help_texts = {
		    'name': _('Nombre del Proveedor'),
		}

	def clean_name(self):
		 name = self.cleaned_data.get('name')
		 return validate_redirect_provider(name)

	def redirect(self):
		name = self.cleaned_data.get('name')
		slug_name = slugify(name)
		return redirect(reverse('detail_provider', kwargs={'slug': slug_name}))

	def __init__(self, *args, **kwargs):
		super(SearchProviderForm, self).__init__(*args, **kwargs)
		for field in self.fields:
			self.fields[field].error_messages.update(default_errors)