from .models import Provider
from django.contrib import admin

class ProviderAdmin(admin.ModelAdmin):
    '''
        Admin View for Provider
    '''
    exclude = ('slug',)
    list_display = ('name','code','email',)
    # list_filter = ('',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    # search_fields = ['']

admin.site.register(Provider, ProviderAdmin)
