from .forms import ProviderForm, SearchProviderForm
from .models import Provider
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.views.generic import FormView, UpdateView, View, DetailView

class CreateProviderView(FormView):
	'''
	    View to create providers
	'''
	form_class = ProviderForm
	success_url = '/'
	template_name = 'form.html'

	def form_valid(self,form):
		form.save()
		# ctx = {'success':'Proveedor Agredado Correctamente','form':form}
		# return render_to_response(self.template_name, ctx, context_instance = RequestContext(self.request))
		return super(CreateProviderView,self).form_valid(form)


def DeleteProviderView(request,slug):
	'''
        View to delete providers
    '''
	provider_to_delete = get_object_or_404(Provider,slug = slug)
	provider_to_delete.delete()
	return redirect('/')


class UpdateProviderView(UpdateView):
	'''
        View to update providers
    '''
	form_class = ProviderForm
	model = Provider
	slug_field = 'slug'
	slug_url_kwarg = 'slug'
	success_url = '/'
	template_name = 'form.html'


class SearchProviderView(View):
	'''
        View contains a list of providers 
        and a form to search providers by code
    '''
	template_name = 'list_provider.html'
	queryset = Provider.objects.all()

	def get(self, request):
		form = SearchProviderForm()
		ctx = {
			'form':form,		
			'providers':self.queryset,
		}
		return render_to_response(self.template_name, ctx, context_instance = RequestContext(request))

	def post(self, request):
		form = SearchProviderForm(request.POST)
		if form.is_valid():
			return form.redirect()
		ctx = {
			'form':form,
			'providers':self.queryset,
		}
		return render_to_response(self.template_name, ctx, context_instance = RequestContext(request))

class DetailProviderView(DetailView):
	context_object_name = 'provider'
	template_name = 'provider_detail.html'
	model = Provider
	slug_field = 'slug'
	slug_url_kwarg = 'slug'