from .views import CreateProviderView, UpdateProviderView, SearchProviderView, DetailProviderView
from django.conf.urls import patterns, url, include

urlpatterns = patterns('',
	url(r'^management/providers/new/$', CreateProviderView.as_view(), name='create_provider'),
	url(r'^management/providers/edit/(?P<slug>.*)/$', UpdateProviderView.as_view(), name='update_provider'),
	url(r'^management/providers/delete/(?P<slug>.*)/$', 'Providers.views.DeleteProviderView', name='delete_provider'),
	url(r'^management/providers/search/$', SearchProviderView.as_view(), name='search_provider'),
	url(r'^management/providers/search/(?P<slug>.*)/$', DetailProviderView.as_view(), name='detail_provider'),
)