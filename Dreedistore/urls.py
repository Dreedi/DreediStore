from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from Products.viewsets import ProductViewSet
from Providers.viewsets import ProviderViewSet

router = routers.DefaultRouter()
router.register(r'products',ProductViewSet)
router.register(r'providers',ProviderViewSet)

urlpatterns = patterns('',    
    url(r'', include('Products.urls')),
    url(r'', include('Providers.urls')),
)

urlpatterns += patterns('',
	url(r'^api/', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
)
