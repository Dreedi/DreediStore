app.service('shoppingCart', ['$filter','$localStorage', function($filter,$localStorage){
	this.addProduct = function(product){
		this.checkStorage();
		var productPrice = parseFloat(product.sale_price);
		var exists = this.productExists(product);
		if(exists){
			var position = $localStorage.dreediStore.products.indexOf(exists);
			$localStorage.dreediStore.products[position].items_to_buy += 1;
		}
		else{
			product.items_to_buy = 1;
			$localStorage.dreediStore.products.push(product);
		}
		$localStorage.dreediStore.total += productPrice;
		console.log($localStorage.dreediStore);
	};
	this.removeProduct = function (product) {
		var productPrice = parseFloat(product.sale_price);
		var exists = this.productExists(product);
		if(exists){
			var position = $localStorage.dreediStore.products.indexOf(exists);
			if(exists.items_to_buy > 1){
				$localStorage.dreediStore.products[position].items_to_buy -= 1;
			}
			else{
				$localStorage.dreediStore.products.splice(position, 1);
			}
			$localStorage.dreediStore.total -= productPrice;
		}
		else{
			alert('El producto no se encuentra en tu carrito de compras.');
		}
		console.log($localStorage.dreediStore);
	};
	this.productExists = function (product) {
		return $filter('filter')(
			$localStorage.dreediStore.products,
			product.slug
		)[0];
	};
	this.getCart = function () {
		return $localStorage.dreediStore;
	};
	this.checkStorage = function () {
		if(!$localStorage.dreediStore){
			$localStorage.dreediStore = {
				products : [],
				total : 0
			};
		}
	};
	this.clearShoppingCart = function () {
		$localStorage.dreediStore = {
			products : [],
			total : 0
		};
	};
}]);

