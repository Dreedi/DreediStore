app.service('productsRequest', ['$http', function($http){
	var apiBaseUrl = '/api/products/';
	this.get = function(fnOK,fnError){
		$http({
			method: 'GET',
			url: apiBaseUrl
		})
		.success(function(data, status, headers, config) {
			fnOK(data);
		})
		.error(function(data, status, headers, config) {
			fnError(data,status);
		});
	};
}]);

