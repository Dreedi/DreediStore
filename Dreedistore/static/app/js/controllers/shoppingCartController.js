app.controller('shoppingCartController', ['$scope', 'shoppingCart', function($scope, shoppingCart){
	function updateCart(){
		$scope.shoppingCart = shoppingCart.getCart();
	}
	$scope.clearCart = function(){
		shoppingCart.clearShoppingCart();
		updateCart();
	};
	$scope.$watch(
		shoppingCart.getCart(),
		updateCart()
	);
}]);

