app.controller('productsListController', ['$scope', 'productsRequest', 'shoppingCart', function ($scope, productsRequest, shoppingCart) {
	$scope.getProducts = function(){
		productsRequest.get(
			function(products) {
				$scope.products = products;
			},
			function(data, status) {
				alert('Ha fallado la petición. Estado HTTP:' + status);
			}
		);
	};
	$scope.addToCart = function(index){
		shoppingCart.addProduct($scope.products[index]);
	};
	$scope.removeFromCart = function(index){
		shoppingCart.removeProduct($scope.products[index]);
	};
	$scope.getProducts();
}]);
