from .forms import ProductForm,SearchProductForm
from .models import Product
from django.shortcuts import get_object_or_404, render_to_response, redirect, get_list_or_404
from django.template import RequestContext
from django.views.generic import FormView, UpdateView, View, DetailView
from .validators import validate_product
from django.utils.text import slugify


class CreateProductView(FormView):
	'''
        View to create products
    '''
	form_class = ProductForm
	success_url = '/'
	template_name = 'form.html'

	def form_valid(self,form):
		form.save()
		# ctx = {'success':'Producto Agredado Correctamente','form':form}
		# return render_to_response(self.template_name, ctx, context_instance = RequestContext(self.request))
		return super(CreateProductView,self).form_valid(form)


def DeleteProductView(request,slug):
	'''
        View to delete products
    '''
	product_to_delete = get_object_or_404(Product,slug = slug)
	product_to_delete.delete()
	return redirect('/')


class UpdateProductView(UpdateView):
	'''
        View to update products
    '''
	form_class = ProductForm
	model = Product
	slug_field = 'slug'
	slug_url_kwarg = 'slug'
	success_url = '/'
	template_name = 'form.html'


class SearchProductsView(View):
	'''
        View contains a list of products 
        and a form for a smart search of products
    '''
	template_name = 'list_products.html'
	queryset = Product.objects.all()

	def get(self, request):
		form = SearchProductForm()
		ctx = {
			'form':form,
			'products':self.queryset,
		}
		return render_to_response(self.template_name,ctx,context_instance = RequestContext(request))

	def post(self, request):
		form = SearchProductForm(request.POST)
		ctx = {
			'form':form,
		}
		if form.is_valid():
			name = request.POST.get('name')
			products =  validate_product(name)
			if products.__class__ is Product:
				products =  [products]
			ctx.update({'products':products})
			return render_to_response(self.template_name, ctx, context_instance = RequestContext(request))
		else:
			ctx.update({'products':self.queryset})
			return render_to_response(self.template_name, ctx, context_instance = RequestContext(request))


class DetailProductView(DetailView):
	template_name = "product_detail.html"
	model = Product
	slug_field = 'slug'
	slug_url_kwarg = 'slug'
	
	def get_context_data(self, **kwargs):
		if 'view' not in kwargs:
		    kwargs['view'] = self
		    kwargs['slug'] = self.kwargs.get('slug')
		return kwargs