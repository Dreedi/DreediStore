from .models import Product
from .validators import default_errors,custom_errors,validate_blank,validate_product
from django import forms
from djangular.forms import NgModelFormMixin, NgFormValidationMixin
from django.shortcuts import get_list_or_404, get_object_or_404
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

class ProductForm(NgFormValidationMixin, NgModelFormMixin, forms.ModelForm):
    '''
        Form for update and create products
    '''
    scope_prefix = 'product'
    form_name = 'productform'

    class Meta:
        model = Product
        exclude = ['slug']
        labels = {
            'name': _('Nombre'),
            'code': _('Codigo'),
            'provider': _('Proveedor'),
            'sale_price': _('Precio de venta'),
            'purchase_price': _('Precio de compra'),
            'minimum_amount': _('Cantidad minima'),
            'stock_amount': _('Cantidad en inventario'),
            'net_content': _('Contenido neto'),
            'measurement_unit': _('Unidad de medida'),
        }
        help_texts = {
            'name': _('Nombre del producto'),
            'code': _('Codigo de barras del producto'),
            'provider': _('Nombre del proveedor del producto'),
            'sale_price': _('Precio de venta del producto'),
            'purchase_price': _('Precio de compra al proveedor'),
            'minimum_amount': _('Si la cantidad en el stock es mas baja, se notificara al administrador'),
            'stock_amount': _('Cantidad actual en inventario'),
            'net_content': _('Contenido neto del producto'),
            'measurement_unit': _('Unidad de medidad del producto'),
        }
        error_messages = {
            'code':{
                'unique':_('Este codigo ya esta asociado a otro producto')
            }
        }

    def clean(self):
        super(ProductForm, self).clean()
        name = self.cleaned_data.get('name')
        net_content = self.cleaned_data.get('net_content')
        measurement_unit = self.cleaned_data.get('measurement_unit')
        full_name = '%s %s %s'%(name, net_content, measurement_unit)
        slug_full_name = slugify(full_name.decode('unicode-escape'))
        try:
            product = Product.objects.get(slug = slug_full_name)
        except Product.DoesNotExist:
            return self.cleaned_data
        else:
            raise forms.ValidationError(custom_errors['unique_product'])

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages.update(default_errors)
            self.fields[field].validators=[validate_blank]


class SearchProductForm(forms.Form):

    name = forms.CharField(
        required=True,
        label='Nombre o Proveedor'
    )

    def clean_name(self):
        name = self.cleaned_data.get('name')
        return validate_product(name)

    def __init__(self, *args, **kwargs):
        super(SearchProductForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages.update(default_errors)
            self.fields[field].validators=[validate_blank]