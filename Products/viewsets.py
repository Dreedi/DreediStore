from .models import Product
from .serializers import ProductSerializer
from rest_framework import viewsets
from django.shortcuts import get_list_or_404

class ProductViewSet(viewsets.ModelViewSet):
	queryset = Product.objects.all()
	serializer_class = ProductSerializer

	def get_queryset(self):
		slug = self.request.query_params.get('slug',None)
		if slug is not None:
			queryset = get_list_or_404(Product, slug = slug)
		else:
			queryset = self.queryset
		return queryset