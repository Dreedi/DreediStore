from django.core.exceptions import ValidationError
from django.db import models
from django.utils.text import slugify
from Providers.models import Provider

MESUREMENT_UNITS = (
	('Kg','Kg'),
	('g','g'),
	('L','L'),
	('ml','ml'),
)

class Product(models.Model):
	'''
        Product model for database
    '''
	name = models.CharField(
		blank=False, 
		max_length=20,
		null=False,
	)

	code = models.BigIntegerField(
		blank=False, 
		null=False,
		unique=True,
	)

	provider = models.ForeignKey(
		Provider,
		blank=False,
		null=False,
	)

	sale_price = models.DecimalField(
		blank=False, 
		decimal_places=2,
		max_digits=10, 
		null=False, 
	)

	purchase_price = models.DecimalField(
		blank=False, 
		decimal_places=2,
		max_digits=10, 
		null=False, 
	)

	minimum_amount = models.IntegerField(
		blank=False, 
		null=False,
	)

	stock_amount = models.IntegerField(
		blank=False, 
		null=False,
	)

	net_content = models.IntegerField(
		blank=False,
		null=False,
	)

	measurement_unit = models.CharField(
		blank=False,
		choices=MESUREMENT_UNITS,
		max_length=20,
		null=False,
	)

	slug = models.SlugField(
		blank=True,
		max_length=50,
		null=True,
		unique=True
	)

	class Meta:
		verbose_name = "Product"
		verbose_name_plural = "Products"

	def slugify_full_name(self):
		full_name = '%s %s %s'%(self.name, self.net_content, self.measurement_unit)
		return slugify(full_name.decode('unicode-escape'))

	def save(self, *args, **kwargs):
		self.slug = self.slugify_full_name()
		super(Product, self).save(*args, **kwargs)

	def __unicode__(self):
		return "%s %s %s" %(self.name, self.net_content, self.measurement_unit)