from .models import Product
from django import forms
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_list_or_404
from django.core.exceptions import MultipleObjectsReturned

default_errors = {
	'blank':_('El campo esta en blanco'),
	'invalid':_('Inserte un valor valido'),
	'invalid_choice':_('Escoje una opcion valida'),
	'max_length':_('Longitud Maxima rebasada'),
	'min_length':_('El Texto es demasiado corto'),
	'required':_('Este campo es requerido'),
}

custom_errors = {
	'unique_product':_('Este producto ya esta registrado'),
	'product_not_exist':_('No se encontraron productos'),
}

def validate_blank(value):
	if str(value).isspace():
		raise forms.ValidationError(default_errors['blank'])

def validate_product(value):
	try:
		products = Product.objects.get(provider__name__iexact = value)
		return products
	except MultipleObjectsReturned:
		products = Product.objects.filter(provider__name__iexact = value)
		return products
	except Product.DoesNotExist:
		try:
			products = Product.objects.get(name__icontains = value)
			return products
		except MultipleObjectsReturned:
			products = Product.objects.filter(name__icontains = value)
			return products
		except Product.DoesNotExist:
			try:
				products = Product.objects.get(slug = slugify(value))
				return products
			except Product.DoesNotExist:
				raise forms.ValidationError(custom_errors['product_not_exist'])