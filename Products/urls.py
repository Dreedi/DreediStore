from .views import CreateProductView,UpdateProductView,SearchProductsView, DetailProductView
from django.conf.urls import patterns, url, include

urlpatterns = patterns('',
	url(r'^management/products/new/$', CreateProductView.as_view(), name='create_product'),
	url(r'^management/products/edit/(?P<slug>.*)/$', UpdateProductView.as_view(), name='update_product'),
	url(r'^management/products/delete/(?P<slug>.*)/$', 'Products.views.DeleteProductView', name='delete_product'),
	url(r'^management/products/search/$', SearchProductsView.as_view(), name='search_products'),
	url(r'^management/products/detail/(?P<slug>.*)/$', DetailProductView.as_view(), name='detail_product'),
)
