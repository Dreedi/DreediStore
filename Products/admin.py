from .models import Product
from django.contrib import admin

def name(self):
    return "%s %s %s" %(self.name, self.net_content, self.measurement_unit)

class ProductAdmin(admin.ModelAdmin):
    '''
        Admin View for Product
    '''
    exclude = ('slug',)
    list_display = (name,'code','provider','sale_price','purchase_price')
   # list_filter = ('',)
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    # readonly_fields = ('',)
    # search_fields = ['']


admin.site.register(Product, ProductAdmin)
