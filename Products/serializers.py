from .models import Product
from rest_framework import serializers

class ProductSerializer(serializers.HyperlinkedModelSerializer):
	provider = serializers.SlugRelatedField(
        read_only=True,
        slug_field='name'
    )
	class Meta:
		model = Product